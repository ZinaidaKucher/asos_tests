package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FilterByBrandPage extends BasePage{

    @FindBy(xpath = "//a[@data-auto-id='loadMoreProducts']")
    private WebElement loadMoreButton;

    @FindBy(xpath = "//p[contains(text(),'Oreal') or contains(text(),'Oréal')]")
    private List<WebElement> productsOfBrandListText;

    @FindBy(xpath = "//progress[@value]")
    private WebElement quantityOfProductsOnPageField;

    public FilterByBrandPage(WebDriver driver) { super(driver); }

    public void clickOnLoadMoreButton() {loadMoreButton.click(); }

    public Integer getQuantityOfProductsOnPageField() {return Integer.parseInt(quantityOfProductsOnPageField.getAttribute("value")); }

    public int getAmountOfProductsOfBrandListText() { return productsOfBrandListText.size(); }

    public WebElement visibilityOfQuantityOfProductsOnPageField() {return quantityOfProductsOnPageField; }



}

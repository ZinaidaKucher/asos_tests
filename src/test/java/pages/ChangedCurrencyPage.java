package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ChangedCurrencyPage extends BasePage {


    @FindBy(xpath = "//span[contains(@data-auto-id,'Price')]")
    private List<WebElement> pricesOfProductsListText;

    public ChangedCurrencyPage(WebDriver driver) { super(driver); }

    public List<WebElement> getPricesOfProductsListText() { return pricesOfProductsListText; }

}

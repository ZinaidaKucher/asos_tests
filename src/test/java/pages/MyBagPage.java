package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MyBagPage extends BasePage {

    @FindBy(xpath = " //li[@class='bag-item-holder']")
    private List<WebElement> linesOfProductsInBagListText;

    @FindBy(xpath = " //span[@class='bag-subtotal-price']")
    private WebElement subTotalPriceField;

    @FindBy(xpath = "//span[@class='bag-item-colour']")
    private WebElement shadeOfProductInBagField;

    @FindBy(xpath = "//button[@class='bag-item-remove']")
    private WebElement deleteProductFromBagButton;

    @FindBy(xpath = "//span[contains(@class,'bag-item-quantity-holder bag')]")
    private WebElement quantityInBagDropdownList;

    @FindBy(xpath = "//li[contains(text(),'4')]")
    private WebElement quantityInBag;

    @FindBy(xpath = "//button[@class='bag-item-edit-update']")
    private WebElement updateQuantityButton;

    @FindBy(xpath = "//a[contains(text(),'Tint')]")
    private WebElement nameOfProductFromSavedItemsToBag;

    @FindBy(xpath = " //button[@class='bag-item-moveToSaved']")
    private WebElement moveToSavedItemsButton;

    @FindBy(xpath = "//a[@aria-label='Bag 4 items']//span[contains(text(),'4')]")
    private WebElement quantityInBagAfterIncreasing;

    @FindBy(xpath = " //div[@class='bag-item-descriptions']")
    private WebElement descriptionOfProductInBagField;

    public MyBagPage(WebDriver driver) { super(driver); }

    public WebElement visibilityOfSubTotalPriceField() {return subTotalPriceField; }

    public String getTextOfSubTotalPriceField() { return subTotalPriceField.getText().replaceAll("[^0-9.]", ""); }

    public int getAmountOfLinesOfProductsInBagListText() { return linesOfProductsInBagListText.size(); }

    public String getTextOfShadeOfProductInBagField() { return shadeOfProductInBagField.getText(); }

    public void getDeleteProductFromBagButton(){deleteProductFromBagButton.click();}

    public void getQuantityInBagDropdownList(){quantityInBagDropdownList.click();}

    public void getQuantityInBag(){quantityInBag.click();}

    public void getUpdateQuantityButton(){updateQuantityButton.click();}

    public String getTextOfNameOfProductFromSavedItemsToBag() { return nameOfProductFromSavedItemsToBag.getText(); }

    public void getMoveToSavedItemsButton(){moveToSavedItemsButton.click();}

    public WebElement visibilityOfQuantityInBagAfterIncreasing() {return quantityInBagAfterIncreasing; }

    public WebElement visibilityOfDescriptionOfProductInBagField() {return descriptionOfProductInBagField; }

}

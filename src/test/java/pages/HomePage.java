package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//a[contains(@href,'|women')]")
    private WebElement shopWomenButton;

    public HomePage(WebDriver driver) { super(driver); }

    public void clickOnShopWomenButton() {shopWomenButton.click(); }
}

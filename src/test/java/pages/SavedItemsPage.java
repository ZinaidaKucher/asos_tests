package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SavedItemsPage extends BasePage {

    @FindBy(xpath = "//a[contains(@aria-label,'Benefit')]//p[contains(text(),'Tint')]")
    private WebElement textOfNameProductInSavedItemsField;

    @FindBy(xpath = "//button[contains(@class,'toBagButton')]")
    private WebElement moveToBagButton;

    @FindBy(xpath = "//button[contains(@class,'deleteButton')]")
    private WebElement deleteFromSavedItemsButton;

    @FindBy(xpath = "//p[contains(text(),'Benefit')]")
    private List<WebElement> productsDeletedFromSavedItemsList;

    @FindBy(xpath = " //h2[contains(text(),'no Saved ')]")
    private WebElement textOfNoProductsInSavedItemsField;

    public SavedItemsPage(WebDriver driver) { super(driver); }

    public String getTextOfNameProductInSavedItemsField() { return textOfNameProductInSavedItemsField.getText(); }

    public void clickOnMoveToBagButton() {moveToBagButton.click(); }

    public WebElement visibilityOfTextOfNameProductInSavedItemsField() {return textOfNameProductInSavedItemsField; }

    public void clickOnDeleteFromSavedItemsButton() {deleteFromSavedItemsButton.click(); }

    public List<WebElement>  getProductsDeletedFromSavedItemsList() {return productsDeletedFromSavedItemsList; }

    public WebElement visibilityOfMoveToBagButton() {return moveToBagButton; }

    public WebElement visibilityOfTextOfNoProductsInSavedItemsField() {return textOfNoProductsInSavedItemsField; }

    public WebElement visibilityOfDeleteFromSavedItemsButton() {return deleteFromSavedItemsButton; }

}


package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {

    @FindBy(xpath = " //div[contains(@class,'layout-aside')]//div[contains(@class,'grid')]//div[@class='colour-size-select']")
    private WebElement selectingShadeDropdownList;

    @FindBy(xpath = "//option[contains(text(),'Cannes')]")
    private WebElement selectingShadeButton;

    @FindBy(xpath = "//div[contains(@class,'fit')]//span[contains(text(),'Add to bag')]")
    private WebElement addToBagButton;

    @FindBy(xpath = "//option[contains(text(),'Berlin')]")
    private WebElement selectingSecondShadeButton;

    @FindBy(xpath = "//option[contains(text(),'Cairo')]")
    private WebElement selectingThirdShadeButton;

    @FindBy(xpath = "//div[@id='minibag-dropdown']//button[@aria-label='Close']")
    private WebElement closeMiniBagButton;

    @FindBy(xpath = "//span[@id='selectColourError']")
    private WebElement addToBagWithoutSelectingShadeErrorMessage;

    public ProductPage(WebDriver driver) { super(driver); }

    public void clickOnSelectingShadeDropdownList() {selectingShadeDropdownList.click(); }

    public WebElement visibilityOfSelectingShadeButton() { return selectingShadeButton; }

    public void clickOnSelectingShadeButton() {selectingShadeButton.click(); }

    public void clickOnAddToBagButton() {addToBagButton.click(); }

    public void clickOnSelectingSecondShadeButton() {selectingSecondShadeButton.click(); }

    public void clickOnSelectingThirdShadeButton() {selectingThirdShadeButton.click(); }

    public WebElement visibilityOfSelectingShadeDropdownList() { return selectingShadeDropdownList; }

    public void getCloseMiniBagButton(){closeMiniBagButton.click();}

    public WebElement visibilityOfCloseMiniBagButton() { return closeMiniBagButton; }

    public WebElement visibilityOfAddToBagButton() { return addToBagButton; }

    public WebElement visibilityOfAddToBagWithoutSelectingShadeErrorMessage() { return addToBagWithoutSelectingShadeErrorMessage; }

    public String getTextOfAddToBagWithoutSelectingShadeErrorMessage() { return addToBagWithoutSelectingShadeErrorMessage.getText(); }

}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MakeupPage extends BasePage {

    @FindBy(xpath = "//div[contains(text(),'Brand')]")
    private WebElement sortingByBrandButton;

    @FindBy(xpath = " //li[@data-dropdown-id='brand']//div[@data-filter-dropdown='true']")
    private WebElement sortByBrandPopup;

    @FindBy(xpath = "//label[contains(@for,'14434')]")
    private WebElement brandSelectButton;

    @FindBy(xpath = "//label[contains(@for,'14434')]//span[contains(@data-auto-id,'Count')]")
    private WebElement quantityOfProductsOfBrand;

    @FindBy(xpath = "//a[contains(@aria-label,'Soft Matte')]")
    private WebElement productNyxOpenButton;

    @FindBy(xpath = "//article[contains(@id,'813')]//button[@data-auto-id='saveForLater']")
    private WebElement addToSavedItemsButton;

    @FindBy(xpath = "//a[contains(@aria-label,'Bene Tint')]")
    private WebElement productBenefitOpenButton;

    public MakeupPage(WebDriver driver) { super(driver); }

    public void clickOnSortingByBrandButton() {sortingByBrandButton.click(); }

    public WebElement visibilityOfSortByBrandPopup() { return sortByBrandPopup; }

    public void clickOnBrandSelectButton() {brandSelectButton.click(); }

    public Integer getQuantityOfProductsOfBrand() { return Integer.parseInt(quantityOfProductsOfBrand.getText().replaceAll("[^0-9]", "")); }

    public void clickOnProductNyxOpenButton() {productNyxOpenButton.click(); }

    public void clickOnAddToSavedItemsButton() {addToSavedItemsButton.click(); }

    public void clickOnProductBenefitOpenButton() {productBenefitOpenButton.click(); }


}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WomenPage extends BasePage{

    @FindBy(xpath = "//nav[@aria-hidden='false']//div[@role='tablist']")
    private WebElement catalogMenu;

    @FindBy(xpath = "//nav[@aria-hidden='false']//span[contains(text(),'Face + ')]")
    private WebElement catalogFaceBodyButton;

    @FindBy(xpath = "//nav[@aria-hidden='false']//a[contains(@href,'/makeup')]")
    private WebElement catalogMakeupButton;

    @FindBy(xpath = "//div[@data-testid='topbar']//button[@data-testid='country-selector-btn']")
    private WebElement selectCountryButton;

    @FindBy(xpath = "//select[@id='currency']")
    private WebElement selectCurrencyDropdownList;

    @FindBy(xpath = "//option[contains(text(),'USD')]")
    private WebElement currencySelectedButton;

    @FindBy(xpath = "//button[@data-testid='save-country-button']")
    private WebElement updatePreferencesButton;

    public WomenPage(WebDriver driver) { super(driver); }

    public WebElement visibilityOfCatalogMenu() { return catalogMenu; }

    public void selectCatalogFaceBodyButton() {catalogFaceBodyButton.click(); }

    public void clickOnCatalogMakeupButton() {catalogMakeupButton.click(); }

    public void clickOnSelectCountryButton() {selectCountryButton.click(); }

    public WebElement visibilityOfSelectCurrencyDropdownList() {return selectCurrencyDropdownList; }

    public void clickOnSelectCurrencyDropdownList() {selectCurrencyDropdownList.click(); }

    public void clickOnCurrencySelectedButton() {currencySelectedButton.click(); }

    public WebElement visibilityOfUpdatePreferencesButton() {return updatePreferencesButton; }

    public void clickOnUpdatePreferencesButton() {updatePreferencesButton.click(); }

}

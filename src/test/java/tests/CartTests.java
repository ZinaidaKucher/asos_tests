package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CartTests extends BaseTest {

    private final String EXPECTED_SHADE_OF_PRODUCT_IN_BAG = "Cannes";
    private final int EXPECTED_AMOUNT_OF_PRODUCTS_DELETED_FROM_BAG = 1;
    private final int INCREASED_QUANTITY_IN_BAG = 4;
    private final String EXPECTED_ERROR_MESSAGE_FOR_ADDING_TO_BAG_WITHOUT_SELECTING_SHADE = "Please select from the available colour and size options";
    private final String  EXPECTED_NAME_OF_PRODUCT_IN_SAVED_ITEMS_MOVED_FROM_BAG = "Bene Tint";

    @Test
    public void checkAddingToCart() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnProductNyxOpenButton();
        getBasePage().waitForPageLoadComplete(30);
        getProductPage().clickOnSelectingShadeDropdownList();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeButton());
        getProductPage().clickOnSelectingShadeButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfAddToBagButton());
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().clickOnViewBagButton();
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfDescriptionOfProductInBagField());
        assertTrue(getMyBagPage().getTextOfShadeOfProductInBagField().contains(EXPECTED_SHADE_OF_PRODUCT_IN_BAG));
    }

    @Test
    public void checkDeletingFromCart() {
        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnProductNyxOpenButton();
        getBasePage().waitForPageLoadComplete(30);
        getProductPage().clickOnSelectingShadeDropdownList();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeButton());
        getProductPage().clickOnSelectingShadeButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfAddToBagButton());
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getProductPage().getCloseMiniBagButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeDropdownList());
        getProductPage().clickOnSelectingShadeDropdownList();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeButton());
        getProductPage().clickOnSelectingSecondShadeButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfAddToBagButton());
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfCloseMiniBagButton());
        getProductPage().getCloseMiniBagButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeDropdownList());
        getProductPage().clickOnSelectingShadeDropdownList();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeButton());
        getProductPage().clickOnSelectingThirdShadeButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfAddToBagButton());
        getProductPage().clickOnAddToBagButton ();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().clickOnViewBagButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfSubTotalPriceField());
        int actualElementsSize = getMyBagPage().getAmountOfLinesOfProductsInBagListText();
        getMyBagPage().getDeleteProductFromBagButton();
        getBasePage().waitForPageLoadComplete(30);
        int actualElementsSizeNew = getMyBagPage().getAmountOfLinesOfProductsInBagListText();
        assertEquals(actualElementsSize-actualElementsSizeNew,EXPECTED_AMOUNT_OF_PRODUCTS_DELETED_FROM_BAG);
    }

    @Test
    public void checkSubTotalInCart() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnProductNyxOpenButton();
        getBasePage().waitForPageLoadComplete(30);
        getProductPage().clickOnSelectingShadeDropdownList();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeButton());
        getProductPage().clickOnSelectingShadeButton();
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().clickOnViewBagButton();
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfSubTotalPriceField());
        double subTotal=Double.parseDouble(getMyBagPage().getTextOfSubTotalPriceField());
        getMyBagPage().getQuantityInBagDropdownList();
        getMyBagPage().getQuantityInBag();
        getMyBagPage().getUpdateQuantityButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfQuantityInBagAfterIncreasing());
        double subTotalNew=Double.parseDouble(getMyBagPage().getTextOfSubTotalPriceField());
        assertEquals(subTotalNew, subTotal*INCREASED_QUANTITY_IN_BAG);
    }

    @Test
    public void checkAddingToCartWithoutSelectingShade() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnProductNyxOpenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfSelectingShadeDropdownList());
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getProductPage().visibilityOfAddToBagWithoutSelectingShadeErrorMessage());
        assertEquals(getProductPage().getTextOfAddToBagWithoutSelectingShadeErrorMessage(), EXPECTED_ERROR_MESSAGE_FOR_ADDING_TO_BAG_WITHOUT_SELECTING_SHADE);
    }

    @Test
    public void checkMovingProductFromCartToSavedItems() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnProductBenefitOpenButton();
        getBasePage().waitForPageLoadComplete(30);
        getProductPage().clickOnAddToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().clickOnViewBagButton();
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfSubTotalPriceField());
        getMyBagPage().getMoveToSavedItemsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().clickOnSavedItemsButton();
        getBasePage().waitVisibilityOf(30, getSavedItemsPage().visibilityOfTextOfNameProductInSavedItemsField());
        assertTrue(getSavedItemsPage().getTextOfNameProductInSavedItemsField().contains(EXPECTED_NAME_OF_PRODUCT_IN_SAVED_ITEMS_MOVED_FROM_BAG));
    }

}

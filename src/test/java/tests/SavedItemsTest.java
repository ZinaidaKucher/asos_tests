package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SavedItemsTest extends BaseTest {

    private final String EXPECTED_NAME_OF_PRODUCT_IN_SAVED_ITEMS = "Benefit";
    private final String EXPECTED_NAME_OF_PRODUCT_IN_BAG_FROM_SAVED_ITEMS = "Tint";


    @Test
    public void checkAddingToSavedItems() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnAddToSavedItemsButton();
        getBasePage().clickOnSavedItemsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getSavedItemsPage().visibilityOfTextOfNameProductInSavedItemsField());
        assertTrue(getSavedItemsPage().getTextOfNameProductInSavedItemsField().contains(EXPECTED_NAME_OF_PRODUCT_IN_SAVED_ITEMS));
    }

    @Test
    public void checkMovingFromSavedItemsToBag() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnAddToSavedItemsButton();
        getBasePage().clickOnSavedItemsButton();
        getBasePage().waitVisibilityOf(30, getSavedItemsPage().visibilityOfMoveToBagButton());
        getSavedItemsPage().clickOnMoveToBagButton();
        getBasePage().waitVisibilityOf(30, getBasePage().visibilityOfViewBagButton());
        getBasePage().clickOnViewBagButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getMyBagPage().visibilityOfDescriptionOfProductInBagField());
        assertTrue(getMyBagPage().getTextOfNameOfProductFromSavedItemsToBag().contains(EXPECTED_NAME_OF_PRODUCT_IN_BAG_FROM_SAVED_ITEMS));

    }

    @Test
    public void checkDeletingFromSavedItems() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnAddToSavedItemsButton();
        getBasePage().clickOnSavedItemsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getSavedItemsPage().visibilityOfDeleteFromSavedItemsButton());
        getSavedItemsPage().clickOnDeleteFromSavedItemsButton();
        getBasePage().waitVisibilityOf(30, getSavedItemsPage().visibilityOfTextOfNoProductsInSavedItemsField());
        getBasePage().waitForPageLoadComplete(30);
        assertTrue(getSavedItemsPage().getProductsDeletedFromSavedItemsList().isEmpty());
    }
}

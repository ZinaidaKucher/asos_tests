package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FilterTest extends BaseTest {

    @Test
    public void checkFilterByBrand() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        getMakeupPage().clickOnSortingByBrandButton();
        getBasePage().waitVisibilityOf(30, getMakeupPage().visibilityOfSortByBrandPopup());
        int quantity = getMakeupPage().getQuantityOfProductsOfBrand();
        getMakeupPage().clickOnBrandSelectButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().scrollPageToBottom();
        getFilterByBrandPage().clickOnLoadMoreButton();
        getBasePage().waitForPageLoadComplete(30);
        int quantityInField=getFilterByBrandPage().getQuantityOfProductsOnPageField();
        assertEquals(quantityInField,quantity);
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getFilterByBrandPage().visibilityOfQuantityOfProductsOnPageField());
        assertEquals(getFilterByBrandPage().getAmountOfProductsOfBrandListText(),quantity);

    }
}

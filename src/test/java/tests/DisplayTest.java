package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DisplayTest extends BaseTest{

    private final String EXPECTED_CURRENCY = "$";

    @Test
    public void checkChangeCurrency() {

        getHomePage().clickOnShopWomenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().clickOnSelectCountryButton();
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfSelectCurrencyDropdownList());
        getWomenPage().clickOnSelectCurrencyDropdownList();
        getWomenPage().clickOnCurrencySelectedButton();
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfUpdatePreferencesButton());
        getWomenPage().clickOnUpdatePreferencesButton();
        getBasePage().waitVisibilityOf(30, getWomenPage().visibilityOfCatalogMenu());
        getWomenPage().selectCatalogFaceBodyButton();
        getWomenPage().clickOnCatalogMakeupButton();
        getBasePage().waitForPageLoadComplete(30);
        for (WebElement webElement : getChangedCurrencyPage().getPricesOfProductsListText()) {
            assertTrue(webElement.getText().contains(EXPECTED_CURRENCY));
        }
    }
}
